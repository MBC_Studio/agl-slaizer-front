import React from 'react';
import {Card, Tooltip} from "antd";
import cl from "./AvailabilityGraph.module.css"
import ConvertService from "../../../service/ConvertService";

const AvailabilityGraph = ({graph}) => {

    const getColor = (value) => {
        if (value > 90) {
            return '#5d9e37';
        }
        if (value > 60) {
            return '#bab042';
        }
        if (value === -1) {
            return '#717171';
        } else {
            return '#842a2a';
        }
    };

    return (
        <Card className={cl.card} title={<span className={cl.title}>{graph?.name}</span>}>
            <div className={cl.graphBox}>
                <div className={cl.graphs}>
                    {
                        graph?.availabilityUnits?.map((g) =>
                            <Tooltip placement="bottom" title={
                                <div className={cl.tooltip}>
                                    <div>{ConvertService.convertDateToDate(g?.timestamp)}</div>
                                    <div>
                                        {
                                            (g?.value === -1)
                                                ? <>Нет данных</>
                                                : <>{g?.value} %</>
                                        }
                                    </div>
                                </div>
                            }>
                                <div className={cl.rectangle} style={{backgroundColor: getColor(g?.value)}}/>
                            </Tooltip>
                        )
                    }
                </div>
                <div className={cl.graphDescription}>
                    <div>90 дней назад</div>
                    <div className={cl.strip}/>
                    <div>100%</div>
                    <div className={cl.strip}/>
                    <div>Сегодня</div>
                </div>
            </div>
        < /Card>
    );
}

export default AvailabilityGraph;