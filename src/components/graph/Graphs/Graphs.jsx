import React from 'react';
import ResponseTimeGraph from "../ResponseTimeGraph/ResponseTimeGraph";
import AvailabilityGraph from "../AvailabilityGraph/AvailabilityGraph";

const Graphs = ({graphs}) => {
    return (
        <div>
            {
                graphs?.map((g) =>
                    <>
                        {
                            (g?.graphType === "AVAILABILITY" || g?.graphType === "COMBINED") &&
                            <AvailabilityGraph graph={g}/>
                        }
                        {
                            (g?.graphType === "RESPONSETIME" || g?.graphType === "COMBINED") &&
                            <ResponseTimeGraph graph={g}/>
                        }
                    </>
                )
            }
        </div>
    );
};

export default Graphs;