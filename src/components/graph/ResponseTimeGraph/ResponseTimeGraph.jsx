import React from 'react';
import {Line} from "@ant-design/charts";
import {Card} from "antd";

const ResponseTimeGraph = ({graph}) => {

    const config = {
        data: graph?.responseTimeUnits?.map((d) => {
            return {
                ms: d.value,
                timestamp: new Date(d.timestamp)
            }
        }),
        xField: 'timestamp',
        yField: 'ms',
        theme: 'dark',
        connectNulls: {
            connect: true,
            connectStroke: '#aaa',
        },
    };
    
    return (
        <div>
            <Card className={"my-5"} title={
                <span className={"text-lg font-semibold uppercase pb-5"}>{graph?.name}</span>
            }>
                <Line {...config}/>
            </Card>
        </div>
    );
};

export default ResponseTimeGraph;