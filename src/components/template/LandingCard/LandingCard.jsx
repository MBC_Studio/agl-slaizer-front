import React from 'react';
import cl from './LandingCard.module.css';

const LandingCard = ({icon, title, description}) => {
    return (
        <div className={cl.block}>
            <div className={cl.titleBox}>
                <img src={icon} alt={"icon"} className={cl.icon}/>
                <div className={cl.title}>{title}</div>
            </div>
            <div className={cl.description}>{description}</div>
        </div>
    );
};

export default LandingCard;