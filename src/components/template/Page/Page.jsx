import React, {useContext} from 'react';
import {Context} from "../../../index";
import cl from "./Page.module.css";
import '../../../index.css';
import {Link} from "react-router-dom";
import {Button} from "antd";

import logo from '../../../images/logo.png'

const Page = ({children}) => {

    const {store} = useContext(Context)

    return (
        <div>
            <div className={cl.headerBox}>
                <div className={cl.header}>
                    <div className={cl.logoBox}>
                        <img src={logo} className={cl.logo} alt={"logo"}/>
                        <div className={cl.logoTitle}>WPA</div>
                    </div>
                    <div className={cl.pageTitleBox}>
                        <Link to={'/'}>Главная</Link>
                        {
                            store.isAuth ?
                                <Link to={'/projects'}>{
                                    store.isAdmin() ?
                                        "Мои проекты"
                                        :
                                        "Проекты"
                                } </Link>
                                :
                                <Link to={'/login'}>Проекты</Link>
                        }
                        {
                            store.isAdmin() &&
                            <>
                                <Link to={'/admin/users'}>
                                    Пользователи
                                </Link>
                                <Link to={'/admin/projects'}>
                                    Все проекты
                                </Link>
                            </>
                        }
                    </div>
                    {
                        store.isAuth ?
                            <Link to={'/account'}>
                                <Button size={"large"}>
                                    Профиль
                                </Button>
                            </Link>
                            :
                            <Link to={'/login'}>
                                <Button size={"large"}>
                                    Войти
                                </Button>
                            </Link>
                    }
                </div>
            </div>

            <div className={cl.children}>
                {children}
            </div>

            <div className={cl.footerBox}>
                <div className={cl.box}>
                    <div className={cl.logoBoxFooter}>
                        <img src={logo} alt={"logo"} className={cl.logoFooter}/>
                        <p>© WebPulse Analytics</p>
                        <p>Все права защищены. 2024</p>
                    </div>
                    <div className={cl.linkBox}>
                        <div>
                            <h4>Страницы</h4>
                            <Link to={"/"} className={cl.link}>Главная</Link>
                            <Link to={"/projects"} className={cl.link}>Проекты</Link>
                        </div>
                        <div>
                            <h4>Документы</h4>
                            <Link to={"https://t.me/alsuuushka"} className={cl.link} target="_blank">Для отправки жалоб и предложений</Link>
                            <Link to={"/"} className={cl.link} target="_blank">Политика конфиденциальности</Link>
                            <Link to={"/"} className={cl.link} target="_blank">Согласие на обработку данных</Link>
                            <Link to={"/"} className={cl.link} target="_blank">Документация</Link>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default Page;