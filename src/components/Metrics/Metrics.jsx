import React, {useContext, useEffect, useState} from 'react';
import {Button, Modal, Space, Table, Tag, Tooltip} from "antd";
import {DeleteOutlined, EditOutlined, ExclamationCircleFilled} from "@ant-design/icons";
import {Context} from "../../index";
import {Link, useParams} from "react-router-dom";

const Metrics = () => {

    const {store} = useContext(Context)
    const {projectId} = useParams();
    const [metrics, setMetrics] = useState([]);

    useEffect(() => {
        const fetchData = async () => {
            const response = await store.metrics.getAll(projectId);
            setMetrics(response)
        }
        fetchData()
    }, [
        store.metrics,
        setMetrics,
        projectId
    ]);

    // Обновление данных
    const updateData = async () => {
        const response = await store.metrics.getAll(projectId);
        setMetrics(response)
    }

    const deleteMetricConfirm = (metricId) => {
        Modal.confirm({
            title: 'Вы уверенны, что хотите удалить метрику?',
            icon: <ExclamationCircleFilled/>,
            content: '',
            okText: 'Удалить',
            okType: 'danger',
            cancelText: 'Отмена',
            onOk() {
                store.metrics
                    .delete(projectId, metricId)
                    .then(updateData)
                //TODO: сделать темную тему, не работает удаление метрики
            },
            onCancel() {
                console.log('Cancel');
            },
        });
    };

    return (
        <div>
            <Button size={"large"} type={"primary"}
                    href={`/projects/${projectId}/metrics/create`}>
                Создать метрику
            </Button>
            <Table className={"mt-10"}
                   dataSource={metrics}
                   rowKey={metric => metric.id}
                   pagination={{position: ['none']}}
                   columns={[
                       {
                           title: 'ID',
                           dataIndex: 'id',
                           key: 'id',
                       },
                       {
                           title: 'Название',
                           dataIndex: 'name',
                           key: 'name',
                       },
                       {
                           title: 'Добавочный путь',
                           dataIndex: 'path',
                           key: 'path',
                           render: (value) => {
                               return <Tag>/{value}</Tag>
                           }
                       },
                       {
                           title: 'Тип метода',
                           dataIndex: 'methodType',
                           key: 'methodType',
                           render: (methodType) => {
                               let color;
                               switch (methodType) {
                                   case "GET":
                                       color = 'green';
                                       break;
                                   case "POST":
                                       color = 'orange';
                                       break;
                                   case "PUT":
                                       color = 'magenta';
                                       break;
                                   case "DELETE":
                                       color = 'red';
                                       break;
                                   case "PATCH":
                                       color = 'gold';
                                       break;
                                   default:
                                       color = 'default';
                                       break;
                               }
                               return (
                                   <Tag color={color} key={methodType}>
                                       {methodType}
                                   </Tag>
                               );
                           },
                       },
                       {
                           title: 'Данные для запроса',
                           dataIndex: 'data',
                           key: 'data',
                           render: (value) => (
                               <Tooltip title={<div>{value.slice(0, 150)}</div>}>
                                   <Tag>
                                       {value.slice(0, 20)}
                                   </Tag>
                               </Tooltip>
                           )
                       },
                       {
                           title: 'Интервал',
                           dataIndex: 'interval',
                           key: 'interval',
                           render: (interval) => {
                               switch (interval) {
                                   case 'ONEMIN':
                                       return '1 минута';
                                   case 'FIVEMIN':
                                       return '5 минут';
                                   case 'FIFTEENMIN':
                                       return '15 минут';
                                   case 'THIRTYMIN':
                                       return '30 минут';
                                   case 'ONEHOUR':
                                       return '1 час';
                                   case 'THREEHOURS':
                                       return '3 часа';
                                   case 'FIVEHOURS':
                                       return '5 часов';
                                   case 'TWELVEHOURS':
                                       return '12 часов';
                                   case 'ONEDAY':
                                       return '1 день';
                                   default:
                                       return '';
                               }
                           }
                       },
                       {
                           title: 'Тип графика',
                           dataIndex: 'graphType',
                           key: 'graphType',
                           render: (graphType) => {
                               let color;
                               let value;
                               switch (graphType) {
                                   case 'AVAILABILITY':
                                       color = 'gold';
                                       value = 'График доступности';
                                       break;
                                   case 'RESPONSETIME':
                                       color = 'blue';
                                       value = 'График времени отклика';
                                       break;
                                   case 'COMBINED':
                                       color = 'green';
                                       value = 'Совместный график';
                                       break;
                                   default:
                                       break;
                               }
                               return (
                                   <Tag color={color}>{value}</Tag>
                               )
                           }
                       },
                       {
                           title: 'Показ пользователям',
                           dataIndex: 'showOnPage',
                           key: 'showOnPage',
                           render: (showOnPage) => {
                               let color;
                               let value;
                               if (showOnPage) {
                                   color = 'green';
                                   value = 'Да';
                               } else {
                                   color = 'red';
                                   value = 'Нет';
                               }
                               return (
                                   <Tag color={color}>{value}</Tag>
                               )
                           }
                       },
                       {
                           title: 'Действия',
                           dataIndex: 'id',
                           key: 'actions',
                           render: (metricId) => (
                               <Space size="middle">
                                   <Link to={`/projects/${projectId}/metrics/update/${metricId}`}>
                                       <EditOutlined/>
                                   </Link>
                                   <DeleteOutlined onClick={() => deleteMetricConfirm(metricId)}/>
                               </Space>
                           ),
                       }
                   ]}
            />
        </div>
    );
};

export default Metrics;