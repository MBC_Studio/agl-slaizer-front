import React, {useContext, useState} from 'react';
import {Button, Card, Space, Switch, Tag} from "antd";
import cl from './SharePage.module.css';
import {Context} from "../../index";
import {Link} from "react-router-dom";

const SharePage = ({projectId, projectCode, sharePage}) => {

    const {store} = useContext(Context)
    const [projectStatus, setProjectStatus] = useState(sharePage?.status);

    const handleSwitchChange = (checked) => {
        const newStatus = checked ? 'ACTIVE' : 'INACTIVE';
        setProjectStatus(newStatus);
        store.sharePages.changeStatus(
            projectId,
            sharePage?.id,
            {
                status: newStatus
            }
        );
    };

    return (
        <div>
            <Space direction="vertical" size={16}>
                <Card title="Информация о странице" extra={<Link to={`/public/${projectCode}`}>Перейти на страницу</Link>} style={{width: 800}}>
                    <div className={cl.infoBox}>
                        <div className={cl.infoTitle}>Название страницы:</div>
                        <div className={cl.infoDesc}>{sharePage?.title}</div>
                    </div>
                    <div className={cl.infoBox}>
                        <div className={cl.infoTitle}>Описание:</div>
                        <div className={cl.infoDesc}>{sharePage?.description}
                        </div>
                    </div>
                    <div className={cl.infoBox}>
                        <div className={cl.infoTitle}>Количество посетителей:</div>
                        <Tag className={cl.infoDesc}>{sharePage?.visits}</Tag>
                    </div>
                    <div className={cl.infoBox}>
                        <div className={cl.infoTitle}>Показывать страницу:</div>
                        <Switch defaultChecked={projectStatus === 'ACTIVE'} onChange={handleSwitchChange} />
                    </div>
                    <Button size={"large"} type={"primary"} className={"mt-10"}
                            href={`/projects/${projectId}/share-pages/update`}>
                        Редактировать страницу
                    </Button>
                </Card>
            </Space>
        </div>
    );
};

export default SharePage;