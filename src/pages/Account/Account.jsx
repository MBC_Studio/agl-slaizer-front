import React, {useContext} from 'react';
import {Button, Card, Form, Input, Modal} from 'antd';
import Page from "../../components/template/Page/Page";

import avatar from "../../images/man-avatar.jpg";
import {ExclamationCircleFilled, FormOutlined} from "@ant-design/icons";
import {Context} from "../../index";
import {observer} from "mobx-react-lite";

const {Meta} = Card;

const Account = () => {

    const [form] = Form.useForm();
    const [, contextHolder] = Modal.useModal();

    const {store} = useContext(Context);

    const changePassword = () => {

        Modal.confirm({
            title: 'Изменение пароля',
            icon: <FormOutlined/>,
            content: (
                <Form
                    form={form}
                    layout={'vertical'}
                    initialValues={{
                        oldPassword: '',
                        newPassword: '',
                        newPasswordConfirm: ''
                    }}
                >
                    <Form.Item
                        label="Старый пароль"
                        name="oldPassword"
                        hasFeedback
                        rules={[
                            {
                                required: true,
                                message: 'Введите старый пароль'
                            },
                            {
                                max: 255,
                                message: 'Максимальная длина пароля 255 символов'
                            }
                        ]}
                    >
                        <Input.Password/>
                    </Form.Item>
                    <Form.Item
                        label="Новый пароль"
                        name="newPassword"
                        hasFeedback
                        rules={[
                            {
                                required: true,
                                message: 'Введите новый пароль'
                            },
                            {
                                max: 255,
                                message: 'Максимальная длина пароля 255 символов'
                            }
                        ]}
                    >
                        <Input.Password/>
                    </Form.Item>
                    <Form.Item
                        name="newPasswordConfirm"
                        label="Подтвердите пароль"
                        dependencies={['newPassword']}
                        hasFeedback
                        rules={[
                            {
                                required: true,
                                message: 'Подтвердите пароль',
                            },
                            ({getFieldValue}) => ({
                                validator(_, value) {
                                    if (!value || getFieldValue('newPassword') === value) {
                                        return Promise.resolve();
                                    }
                                    return Promise.reject(new Error('Пароли не совпадают'));
                                },
                            }),
                        ]}
                    >
                        <Input.Password/>
                    </Form.Item>
                </Form>
            ),
            okText: 'Сохранить',
            okType: 'primary',
            cancelText: 'Отмена',
            onOk: async () => {
                try {
                    const values = await form?.validateFields();
                    form?.resetFields();
                    await store.users.changePassword(values);
                } catch (error) {
                    console.log('Failed:', error);
                }
            },
            onCancel() {
                console.log('Cancel');
            },
        });
    };

    const showDeleteConfirm = () => {
        Modal.confirm({
            title: 'Вы уверенны, что хотите выйти из аккаунта?',
            icon: <ExclamationCircleFilled/>,
            content: '',
            okText: 'Да, выйти',
            okType: 'danger',
            cancelText: 'Отмена',
            onOk() {
                store.users.logout();
                //TODO: сделать темную тему
            },
            onCancel() {
                console.log('Cancel');
            },
        });
    };

    return (
        <div>
            <Page>
                <div className={"flex justify-center pt-20"}>
                    <Card
                        hoverable
                        style={{
                            width: 320,
                        }}
                        cover={<img alt="avatar" src={avatar}/>}
                    >
                        <Meta title={<div className={'text-xl font-bold'}>{store?.user?.username}</div>}
                              description={
                                  <div className={"text-lg"}>{store?.user?.email}</div>
                              }
                        />
                        <Button type="primary" htmlType="submit" size={"large"} block className={"mt-10"}
                                onClick={changePassword}>
                            Изменить пароль
                        </Button>
                        <Button type="primary" htmlType="submit" size={"large"} block danger className={"mt-2"}
                                onClick={showDeleteConfirm}>
                            Выйти из аккаунта
                        </Button>
                    </Card>
                </div>
                {contextHolder}
            </Page>
        </div>
    );
};

export default observer(Account);