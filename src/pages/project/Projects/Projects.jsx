import React, {useContext, useEffect, useState} from 'react';
import Page from "../../../components/template/Page/Page";
import {Button, Modal, Space, Table, Tag} from "antd";
import {DeleteOutlined, EditOutlined, ExclamationCircleFilled, EyeOutlined} from "@ant-design/icons";
import {Context} from "../../../index";
import {Link} from "react-router-dom";
import ConvertService from "../../../service/ConvertService";

const Projects = () => {

    const {store} = useContext(Context)
    const [projects, setProjects] = useState([]);

    useEffect(() => {
        const fetchData = async () => {
            const response = await store.projects.getAllByUser();
            setProjects(response)
        }
        fetchData()
    }, [
        store.projects,
        setProjects
    ]);

    // Обновление данных
    const updateData = async () => {
        const response = await store.projects.getAllByUser();
        setProjects(response)
    }

    const deleteProjectConfirm = (id) => {
        Modal.confirm({
            title: 'Вы уверенны, что хотите удалить проект?',
            icon: <ExclamationCircleFilled/>,
            content: '',
            okText: 'Удалить',
            okType: 'danger',
            cancelText: 'Отмена',
            onOk() {
                store.projects
                    .delete(id)
                    .then(updateData)
                //TODO: сделать темную тему
            },
            onCancel() {
                console.log('Cancel');
            },
        });
    };

    return (
        <div>
            <Page>
                <Button className={"mt-10"} size={"large"} type={"primary"} href={"/projects/create"}>
                    Создать проект
                </Button>
                <Table className={"mt-10"}
                       dataSource={projects}
                       rowKey={project => project.id}
                       pagination={{position: ['none']}}
                       columns={[
                           {
                               title: 'ID',
                               dataIndex: 'id',
                               key: 'id',
                           },
                           {
                               title: 'Название',
                               dataIndex: 'name',
                               key: 'name',
                           },
                           {
                               title: 'Код',
                               dataIndex: 'code',
                               key: 'code',
                           },
                           {
                               title: 'Ссылка',
                               dataIndex: 'baseUrl',
                               key: 'baseUrl',
                               render: (baseUrl) => <Link to={baseUrl} target={"_blank"}>{baseUrl}</Link>,
                           },
                           {
                               title: 'Статус',
                               key: 'status',
                               dataIndex: 'status',
                               render: (status) => {
                                   let color;
                                   if (status === 'ACTIVE') {
                                       color = 'green';
                                   }
                                   if (status === 'BLOCKED') {
                                       color = 'volcano';
                                   }
                                   return (
                                       <Tag color={color} key={status}>
                                           {status.toUpperCase()}
                                       </Tag>
                                   );
                               },
                           },
                           {
                               title: 'Время завершения',
                               dataIndex: 'endTime',
                               key: 'endTime',
                               render: (endTime) => {
                                   return (
                                       <div>{ConvertService.convertDateToDateTime(endTime)}</div>
                                   )
                               },
                           },
                           {
                               title: 'Действия',
                               dataIndex: 'id',
                               key: 'actions',
                               render: (id) => (
                                   <Space size="middle">
                                       <Link to={`/projects/${id}`}>
                                           <EyeOutlined/>
                                       </Link>
                                       <Link to={`/projects/${id}/update`}>
                                           <EditOutlined/>
                                       </Link>
                                       <DeleteOutlined onClick={() => deleteProjectConfirm(id)}/>
                                   </Space>
                               ),
                           }
                       ]}
                />
            </Page>
        </div>
    );
};

export default Projects;