import React, {useContext, useEffect} from 'react';
import Page from "../../../components/template/Page/Page";
import {Button, Card, Form, Input} from "antd";
import {Context} from "../../../index";
import {useNavigate, useParams} from "react-router-dom";


const CreateUpdateProject = () => {

    const {projectId} = useParams();
    const {store} = useContext(Context);
    const [form] = Form.useForm();
    const projectTitle = Form.useWatch('name', form);
    const navigate = useNavigate();

    useEffect(() => {
        if (projectId) {
            const fetchData = async () => {
                const response = await store.projects.getById(projectId);
                form.setFieldsValue({
                    name: response?.name,
                    code: response?.code,
                    baseUrl: response?.baseUrl,
                });
            };
            fetchData();
        }
    }, [form, projectId, store.projects]);

    return (
        <Page>
            <Card className={'max-w-4xl mx-auto mt-48'}
                  title={projectId ? 'Редактирование проекта ' + projectTitle : 'Создание проекта'}
            >
                <Form
                    form={form}
                    layout={'vertical'}
                    initialValues={{
                        name: '',
                        code: '',
                        baseUrl: ''
                    }}
                    onFinish={async (data) => {
                        if (!projectId) {
                            if (await store.projects.create(data)) {
                                navigate('/projects');
                            }
                        } else {
                            if (await store.projects.edit(projectId, data)) {
                                navigate('/projects');
                            }
                        }
                    }
                    }
                >
                    <Form.Item
                        label="Название"
                        name="name"
                        hasFeedback
                        rules={[
                            {
                                required: true,
                                message: 'Введите название'
                            },
                            {
                                min: 2,
                                message: 'Минимальная длина названия 2 символов'
                            },
                            {
                                max: 255,
                                message: 'Максимальная длина названия 255 символов'
                            }
                        ]}
                    >
                        <Input/>
                    </Form.Item>
                    <Form.Item
                        label="Код проекта"
                        name="code"
                        hasFeedback
                        rules={[
                            {
                                required: true,
                                message: 'Введите код проекта'
                            },
                            {
                                min: 3,
                                message: 'Минимальная длина кода 3 символов'
                            },
                            {
                                max: 255,
                                message: 'Максимальная длина кода 255 символов'
                            },
                            {
                                pattern: /^[a-zA-Z0-9-]+$/,
                                message: 'Код проекта должен содержать только английские символы, цифры и тире'
                            }
                        ]}
                    >
                        <Input/>
                    </Form.Item>
                    <Form.Item
                        label="Ссылка на проект"
                        name="baseUrl"
                        hasFeedback
                        rules={[
                            {
                                required: true,
                                message: 'Введите ссылку на проект'
                            },
                            {
                                min: 3,
                                message: 'Минимальная длина ссылки 3 символов'
                            },
                            {
                                max: 255,
                                message: 'Максимальная длина ссылки 255 символов'
                            },
                        ]}
                    >
                        <Input placeholder="https://example.ru"/>
                    </Form.Item>
                    <Form.Item>
                        <Button type="primary" htmlType="submit" size={"large"} block>
                            {projectId ? 'Сохранить' : 'Создать'}
                        </Button>
                    </Form.Item>
                </Form>
            </Card>
        </Page>
    );
};

export default CreateUpdateProject;