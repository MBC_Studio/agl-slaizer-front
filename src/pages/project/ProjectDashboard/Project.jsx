import React, {useContext, useEffect, useState} from 'react';
import Page from "../../../components/template/Page/Page";
import {Card} from "antd";
import {Link, useParams} from "react-router-dom";
import Metrics from "../../../components/Metrics/Metrics";
import SharePage from "../../../components/SharePage/SharePage";
import {Context} from "../../../index";
import Graphs from "../../../components/graph/Graphs/Graphs";
import ConvertService from "../../../service/ConvertService";
import cl from "../../sharePage/SharePageDashboard/SharePage.module.css";

const Project = () => {

    const {store} = useContext(Context);
    const {projectId} = useParams();
    const [project, setProject] = useState();

    const ProjectList = [
        {
            key: 'graphs',
            label: 'Графики',
        },
        {
            key: 'metrics',
            label: 'Метрики',
        },
        {
            key: 'shareProject',
            label: 'Публичная страница',
        },
    ];

    const contentList = {
        graphs: <Graphs graphs={project?.graphs}/>,
        metrics: <Metrics/>,
        shareProject: <SharePage projectId={project?.id} sharePage={project?.sharePage} projectCode={project?.code}/>,
    };

    const [activeTab, setActiveTab] = useState('graphs');
    const onChange = (key) => {
        setActiveTab(key);
    };


    useEffect(() => {
        const fetchData = async () => {
            const response = await store.projects.getDashboard(projectId);
            setProject(response)
        }
        fetchData()
    }, [
        store.projects,
        setProject,
        projectId
    ]);

    return (
        <Page>
            <div className={cl.infoBox}>
                <div className={cl.infoTitle}>Проект:</div>
                <div className={cl.infoDesc}>{project?.name}</div>
            </div>
            <div className={cl.infoBox}>
                <div className={cl.infoTitle}>Код проекта:</div>
                <div className={cl.infoDesc}>{project?.code}</div>
            </div>
            <div className={cl.infoBox}>
                <div className={cl.infoTitle}>Ссылка на проект:</div>
                <Link to={project?.baseUrl} className={cl.infoDesc} target={"_blank"}>{project?.baseUrl}</Link>
            </div>
            <div className={cl.infoBox}>
                <div className={cl.infoTitle}>Дата завершения сбора статистики:</div>
                <div className={cl.infoDesc}>{ConvertService.convertDateToDateTime(project?.endTime)}</div>
            </div>
            <Card
                style={{
                    width: '100%',
                }}
                tabList={ProjectList}
                activeTabKey={activeTab}
                onTabChange={onChange}
                className={"mt-10"}
                tabProps={{
                    size: 'middle',
                }}
            >
                {contentList[activeTab]}
            </Card>

        </Page>
    );
};

export default Project;