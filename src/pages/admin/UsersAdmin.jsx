import React, {useContext, useEffect, useState} from 'react';
import Page from "../../components/template/Page/Page";
import {Modal, Space, Table, Tag} from "antd";
import {DeleteOutlined, EditOutlined, ExclamationCircleFilled} from "@ant-design/icons";
import {Context} from "../../index";
import {Link} from "react-router-dom";

const UsersAdmin = () => {

    const {store} = useContext(Context)
    const [users, setUsers] = useState([]);

    useEffect(() => {
        const fetchData = async () => {
            const response = await store.users.getAll();
            setUsers(response)
        }
        fetchData()
    }, [
        store.users,
        setUsers
    ]);

    // Обновление данных
    const updateData = async () => {
        const response = await store.users.getAll();
        setUsers(response)
    }

    const deleteUserConfirm = (id) => {
        Modal.confirm({
            title: 'Вы уверенны, что хотите удалить пользователя?',
            icon: <ExclamationCircleFilled/>,
            content: '',
            okText: 'Удалить',
            okType: 'danger',
            cancelText: 'Отмена',
            onOk() {
                store.users
                    .delete(id)
                    .then(updateData)
            },
            onCancel() {
                console.log('Cancel');
            },
        });
    };

    return (
        <div>
            <Page>
                <Table className={"mt-10"}
                       dataSource={users}
                       rowKey={user => user.id}
                       pagination={{position: ['none']}}
                       columns={[
                           {
                               title: 'ID',
                               dataIndex: 'id',
                               key: 'id',
                           },
                           {
                               title: 'Имя',
                               dataIndex: 'username',
                               key: 'username',
                           },
                           {
                               title: 'Почта',
                               dataIndex: 'email',
                               key: 'email',
                           },
                           {
                               title: 'Роль',
                               dataIndex: 'role',
                               key: 'role',
                               render: (role) => {
                                   let color;
                                   if (role === 'ROLE_ADMIN') {
                                       color = 'orange';
                                   }
                                   if (role === 'ROLE_USER') {
                                       color = 'blue';
                                   }
                                   return (
                                       <Tag color={color} key={role}>
                                           {role.toUpperCase()}
                                       </Tag>
                                   );
                               },
                           },
                           {
                               title: 'Статус',
                               key: 'status',
                               dataIndex: 'status',
                               render: (status) => {
                                   let color;
                                   if (status === 'ACTIVE') {
                                       color = 'green';
                                   }
                                   if (status === 'BLOCKED') {
                                       color = 'volcano';
                                   }
                                   return (
                                       <Tag color={color} key={status}>
                                           {status.toUpperCase()}
                                       </Tag>
                                   );
                               },
                           },
                           {
                               title: 'Действия',
                               dataIndex: 'id',
                               key: 'actions',
                               render: (id) => (
                                   <Space size="middle">
                                       <Link to={`/admin/users/${id}/update`}>
                                           <EditOutlined/>
                                       </Link>
                                       <DeleteOutlined onClick={() => deleteUserConfirm(id)}/>
                                   </Space>
                               ),
                           }
                       ]}
                />
            </Page>
        </div>
    );
};

export default UsersAdmin;