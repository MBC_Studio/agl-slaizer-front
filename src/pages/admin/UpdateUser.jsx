import React, {useContext, useEffect} from 'react';
import {useNavigate, useParams} from "react-router-dom";
import {Context} from "../../index";
import {Button, Card, Form, Input, message, Select} from "antd";
import Page from "../../components/template/Page/Page";

const UpdateUser = () => {
    const {userId} = useParams();
    const {store} = useContext(Context);
    const [form] = Form.useForm();
    const userTitle = Form.useWatch('username', form);
    const navigate = useNavigate();
    const {Option} = Select;

    useEffect(() => {
        if (userId) {
            const fetchData = async () => {
                const response = await store.users.getById(userId);
                form.setFieldsValue({
                    username: response?.username,
                    email: response?.email,
                    status: response?.status,
                    role: response?.role,
                });
            };
            fetchData();
        }
    }, [form, userId, store.users]);

    return (
        <Page>
            <Card className={'max-w-4xl mx-auto mt-48'}
                  title={'Редактирование пользователя "' + userTitle +'"'}
            >
                <Form
                    form={form}
                    layout={'vertical'}
                    initialValues={{
                        username: '',
                        email: '',
                        status: '',
                        role: '',
                    }}
                    onFinish={async (data) => {
                        if (!userId) {
                            navigate("/404")
                            message.error("Пользователь не найден")
                        } else {
                            if (await store.users.edit(userId, data)) {
                                navigate(`/admin/users`);
                            }
                        }
                    }
                    }
                >
                    <Form.Item
                        label="Имя"
                        name="username"
                        hasFeedback
                        rules={[
                            {
                                required: true,
                                message: 'Введите имя пользователя'
                            },
                            {
                                min: 4,
                                message: 'Минимальная длина имени 4 символа'
                            },
                            {
                                max: 50,
                                message: 'Максимальная длина имени 50 символов'
                            }
                        ]}
                    >
                        <Input/>
                    </Form.Item>
                    <Form.Item
                        label="Почта"
                        name="email"
                        hasFeedback
                        rules={[
                            {
                                required: true,
                                message: 'Введите почту'
                            },
                            {
                                min: 8,
                                message: 'Минимальная длина почты 8 символов'
                            },
                            {
                                max: 255,
                                message: 'Максимальная длина почты 255 символов'
                            },
                        ]}
                    >
                        <Input/>
                    </Form.Item>
                    <Form.Item
                        label="Статус"
                        name="status"
                        hasFeedback
                    >
                        <Select
                            allowClear
                        >
                            <Option value="ACTIVE">Активен</Option>
                            <Option value="BLOCKED">Заблокирован</Option>
                        </Select>
                    </Form.Item>
                    <Form.Item
                        label="Роль"
                        name="role"
                        hasFeedback
                    >
                        <Select
                            allowClear
                        >
                            <Option value="ROLE_USER">Пользователь</Option>
                            <Option value="ROLE_ADMIN">Администратор</Option>
                        </Select>
                    </Form.Item>
                    <Form.Item>
                        <Button type="primary" htmlType="submit" size={"large"} block>
                            Сохранить
                        </Button>
                    </Form.Item>
                </Form>
            </Card>
        </Page>
    );
};

export default UpdateUser;