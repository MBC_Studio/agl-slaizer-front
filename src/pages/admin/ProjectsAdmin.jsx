import React, {useContext, useEffect, useState} from 'react';
import Page from "../../components/template/Page/Page";
import {Button, Form, Modal, Select, Space, Table, Tag} from "antd";
import {
    CloseCircleOutlined,
    DeleteOutlined,
    EditOutlined,
    ExclamationCircleFilled,
    EyeOutlined,
    FormOutlined
} from "@ant-design/icons";
import {Context} from "../../index";
import {Link} from "react-router-dom";
import ConvertService from "../../service/ConvertService";

const ProjectsAdmin = () => {

    const {store} = useContext(Context)
    const [projects, setProjects] = useState([]);
    const [form] = Form.useForm();

    useEffect(() => {
        const fetchData = async () => {
            const response = await store.projects.getAll();
            setProjects(response)
        }
        fetchData()
    }, [
        store.projects,
        setProjects
    ]);

    // Обновление данных
    const updateData = async () => {
        const response = await store.projects.getAll();
        setProjects(response)
    }

    const deleteProjectConfirm = (id) => {
        Modal.confirm({
            title: 'Вы уверенны, что хотите удалить проект?',
            icon: <ExclamationCircleFilled/>,
            content: '',
            okText: 'Удалить',
            okType: 'danger',
            cancelText: 'Отмена',
            onOk() {
                store.projects
                    .delete(id)
                    .then(updateData)
            },
            onCancel() {
                console.log('Cancel');
            },
        });
    };

    const changeProjectStatusConfirm = (id, status) => {

        Modal.confirm({
            title: 'Изменение статуса проекта',
            icon: <FormOutlined/>,
            content: (
                <Form
                    form={form}
                    layout={'vertical'}
                    initialValues={{
                        status: status,
                    }}
                >
                    <Form.Item
                        name="status"
                        label="Статус проекта"
                        hasFeedback
                    >
                        <Select
                            options={[
                                {
                                    label: <Tag color="green">Активен</Tag>,
                                    value: 'ACTIVE',
                                },
                                {
                                    label: <Tag color="red">Заблокирован</Tag>,
                                    value: 'BLOCKED',
                                }
                            ]}
                        />
                    </Form.Item>
                </Form>
            ),
            okText: 'Сохранить',
            okType: 'primary',
            cancelText: 'Отмена',
            onOk: async () => {
                try {
                    const values = await form?.validateFields();
                    form?.resetFields();
                    await store.projects.changeStatus(id, values);
                    await updateData();
                } catch (error) {
                    console.log('Failed:', error);
                }
            },
            onCancel() {
                console.log('Cancel');
            },
        });
    };

    return (
        <div>
            <Page>
                <Button className={"mt-10"} size={"large"} type={"primary"} href={"/projects/create"}>
                    Создать проект
                </Button>
                <Table className={"mt-10"}
                       dataSource={projects}
                       rowKey={project => project.id}
                       pagination={{position: ['none']}}
                       columns={[
                           {
                               title: 'ID',
                               dataIndex: 'id',
                               key: 'id',
                           },
                           {
                               title: 'Пользователь',
                               dataIndex: 'user.username',
                               key: 'user.username',
                               render: (value, record) =>
                                   record?.user?.username

                           },
                           {
                               title: 'Название',
                               dataIndex: 'name',
                               key: 'name',
                           },
                           {
                               title: 'Код',
                               dataIndex: 'code',
                               key: 'code',
                           },
                           {
                               title: 'Ссылка',
                               dataIndex: 'baseUrl',
                               key: 'baseUrl',
                               render: (baseUrl) => <Link to={baseUrl} target={"_blank"}>{baseUrl}</Link>,
                           },
                           {
                               title: 'Статус',
                               key: 'status',
                               dataIndex: 'status',
                               render: (status) => {
                                   let color;
                                   if (status === 'ACTIVE') {
                                       color = 'green';
                                   }
                                   if (status === 'BLOCKED') {
                                       color = 'volcano';
                                   }
                                   return (
                                       <Tag color={color} key={status}>
                                           {status.toUpperCase()}
                                       </Tag>
                                   );
                               },
                           },
                           {
                               title: 'Время завершения',
                               dataIndex: 'endTime',
                               key: 'endTime',
                               render: (endTime) => {
                                   return (
                                       <div>{ConvertService.convertDateToDateTime(endTime)}</div>
                                   )
                               },
                           },
                           {
                               title: 'Действия',
                               dataIndex: 'id',
                               key: 'actions',
                               render: (value, record) => (
                                   <Space size="middle">
                                       <Link to={`/projects/${value}`}>
                                           <EyeOutlined/>
                                       </Link>
                                       <Link to={`/projects/${value}/update`}>
                                           <EditOutlined/>
                                       </Link>
                                       <CloseCircleOutlined onClick={() => changeProjectStatusConfirm(value, record?.status)}/>
                                       <DeleteOutlined onClick={() => deleteProjectConfirm(value)}/>
                                   </Space>
                               ),
                           }
                       ]}
                />
            </Page>
        </div>
    );
};

export default ProjectsAdmin;