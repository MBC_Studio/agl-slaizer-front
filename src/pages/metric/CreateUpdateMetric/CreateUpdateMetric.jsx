import React, {useContext, useEffect} from 'react';
import {useNavigate, useParams} from "react-router-dom";
import {Context} from "../../../index";
import {Button, Card, Form, Input, Select, Tag} from "antd";
import Page from "../../../components/template/Page/Page";
import jsonlint from 'jsonlint-mod';

const CreateUpdateMetric = () => {

    const {metricId, projectId} = useParams();
    const {store} = useContext(Context);
    const [form] = Form.useForm();
    const metricTitle = Form.useWatch('name', form);
    const navigate = useNavigate();
    const {Option} = Select;
    const {TextArea} = Input;

    // Функция для проверки синтаксиса JSON
    const validateJSONSyntax = (rule, value, callback) => {
        if (!value) {
            callback();
            return;
        }
        try {
            jsonlint.parse(value);
            callback();
        } catch (error) {
            callback('Неверный синтаксис JSON');
        }
    };

    useEffect(() => {
        if (metricId) {
            const fetchData = async () => {
                const response = await store.metrics.getById(projectId, metricId);
                form.setFieldsValue({
                    name: response?.name,
                    path: response?.path,
                    methodType: response?.methodType,
                    data: response?.data,
                    interval: response?.interval,
                    graphType: response?.graphType,
                    showOnPage: response?.showOnPage?.toString(),
                })
            }
            fetchData()
        }
    }, [form, metricId, store.metrics, projectId]);

    return (
        <Page>
            <Card className={'max-w-4xl mx-auto mt-10'}
                  title={metricId ? 'Редактирование метрики ' + metricTitle : 'Создание метрики'}
            >
                <Form
                    form={form}
                    layout={'vertical'}
                    initialValues={{
                        name: '',
                        path: '',
                        methodType: 'GET',
                        data: '',
                        interval: 'ONEMIN',
                        graphType: 'AVAILABILITY',
                        showOnPage: 'true'
                    }}
                    onFinish={async (data) => {
                        if (!metricId) {
                            if (await store.metrics.create(projectId, data)) {
                                navigate(`/projects/${projectId}`);
                            }
                        } else {
                            if (await store.metrics.edit(projectId, metricId, data)) {
                                navigate(`/projects/${projectId}`);
                            }
                        }
                    }
                    }
                >
                    <Form.Item
                        label="Название"
                        name="name"
                        hasFeedback
                        rules={[
                            {
                                required: true,
                                message: 'Введите название'
                            },
                            {
                                min: 3,
                                message: 'Минимальная длина названия 3 символов'
                            },
                            {
                                max: 100,
                                message: 'Максимальная длина названия 100 символов'
                            }
                        ]}
                    >
                        <Input/>
                    </Form.Item>
                    <Form.Item
                        label="Добавочный путь"
                        name="path"
                        hasFeedback
                        rules={[
                            {
                                max: 255,
                                message: 'Максимальная длина пути 255 символов'
                            },
                            // {
                            //     pattern: /^[a-zA-Z0-9-]+$/,
                            //     message: 'Путь должен содержать только английские символы, цифры и тире'
                            // }
                        ]}
                    >
                        <Input addonBefore="/"/>
                    </Form.Item>
                    <Form.Item
                        label="Тип запроса"
                        name="methodType"
                        hasFeedback
                        rules={[
                            {
                                required: true,
                                message: 'Введите тип запроса'
                            },
                        ]}>
                        <Select
                            allowClear
                        >
                            <Option value="GET">GET</Option>
                            <Option value="POST">POST</Option>
                            <Option value="PUT">PUT</Option>
                            <Option value="DELETE">DELETE</Option>
                            <Option value="PATCH">PATCH</Option>
                        </Select>
                    </Form.Item>
                    <Form.Item
                        label="Данные для запроса"
                        name="data"
                        hasFeedback
                        rules={[
                            {
                                max: 1000,
                                message: 'Максимальная длина данных 1000 символов'
                            },
                            {
                                pattern: /^[a-zA-Zа-яА-Я0-9{}:"',.\s]*$/,
                                message: 'Данные должны быть в формате JSON'
                            },
                            {
                                validator: validateJSONSyntax
                            }
                        ]}
                    >
                        <TextArea rows={4}/>
                    </Form.Item>
                    <Form.Item
                        label="Интервал между запросами"
                        name="interval"
                        hasFeedback
                        rules={[
                            {
                                required: true,
                                message: 'Введите интервал'
                            },
                        ]}>
                        <Select
                            allowClear
                        >
                            <Option value="ONEMIN">1 минута</Option>
                            <Option value="FIVEMIN">5 минут</Option>
                            <Option value="FIFTEENMIN">15 минут</Option>
                            <Option value="THIRTYMIN">30 минут</Option>
                            <Option value="ONEHOUR">1 час</Option>
                            <Option value="THREEHOURS">3 часа</Option>
                            <Option value="FIVEHOURS">5 часов</Option>
                            <Option value="TWELVEHOURS">12 часов</Option>
                            <Option value="ONEDAY">1 день</Option>
                        </Select>
                    </Form.Item>
                    <Form.Item
                        label="Тип графика"
                        name="graphType"
                        hasFeedback
                        rules={[
                            {
                                required: true,
                                message: 'Введите тип графика'
                            },
                        ]}>
                        <Select
                            allowClear
                        >
                            <Option value="AVAILABILITY">График доступности</Option>
                            <Option value="RESPONSETIME">График времени отклика</Option>
                            <Option value="COMBINED">Совместный график</Option>
                        </Select>
                    </Form.Item>
                    <Form.Item
                        label="Показ на публичной странице"
                        name="showOnPage"
                        hasFeedback
                        rules={[
                            {
                                required: true,
                                message: 'Выберите показ'
                            },
                        ]}>
                        <Select
                            allowClear
                        >
                            <Option value="true">
                                <Tag color={"green"}>Да</Tag>
                            </Option>
                            <Option value="false">
                                <Tag color={"red"}>Нет</Tag>
                            </Option>
                        </Select>
                    </Form.Item>
                    <Form.Item>
                        <Button type="primary" htmlType="submit" size={"large"} block>
                            {metricId ? 'Сохранить' : 'Создать'}
                        </Button>
                    </Form.Item>
                </Form>
            </Card>
        </Page>
    );
};

export default CreateUpdateMetric;