import React, {useContext, useEffect, useState} from 'react';
import {useNavigate, useParams} from "react-router-dom";
import {Context} from "../../../index";
import {Button, Card, Form, Input, message} from "antd";
import Page from "../../../components/template/Page/Page";
import TextArea from "antd/es/input/TextArea";

const UpdateSharePage = () => {
    const {projectId} = useParams();
    const [sharePageId, setSharePageId] = useState();
    const {store} = useContext(Context);
    const [form] = Form.useForm();
    const sharePageTitle = Form.useWatch('title', form);
    const navigate = useNavigate();

    useEffect(() => {
        if (projectId) {
            const fetchData = async () => {
                const response = await store.sharePages.getById(projectId);
                setSharePageId(response?.id)
                form.setFieldsValue({
                    title: response?.title,
                    description: response?.description,
                });
            };
            fetchData();
        }
    }, [form, projectId, store.sharePages]);

    return (
        <Page>
            <Card className={'max-w-4xl mx-auto mt-48'}
                  title={'Редактирование страницы "' + sharePageTitle +'"'}
            >
                <Form
                    form={form}
                    layout={'vertical'}
                    initialValues={{
                        title: '',
                        description: '',
                    }}
                    onFinish={async (data) => {
                        if (!sharePageId) {
                            navigate("/404")
                            message.error("Публичная страница не найдена")
                        } else {
                            if (await store.sharePages.edit(projectId, sharePageId, data)) {
                                navigate(`/projects/${projectId}`);
                            }
                        }
                    }
                    }
                >
                    <Form.Item
                        label="Название"
                        name="title"
                        hasFeedback
                        rules={[
                            {
                                required: true,
                                message: 'Введите название'
                            },
                            {
                                min: 3,
                                message: 'Минимальная длина названия 3 символов'
                            },
                            {
                                max: 60,
                                message: 'Максимальная длина названия 60 символов'
                            }
                        ]}
                    >
                        <Input/>
                    </Form.Item>
                    <Form.Item
                        label="Описание"
                        name="description"
                        hasFeedback
                        rules={[
                            {
                                required: true,
                                message: 'Введите описание'
                            },
                            {
                                min: 5,
                                message: 'Минимальная длина описания 5 символов'
                            },
                            {
                                max: 1000,
                                message: 'Максимальная длина описания 1000 символов'
                            },
                        ]}
                    >
                        <TextArea rows={4}/>
                    </Form.Item>
                    <Form.Item>
                        <Button type="primary" htmlType="submit" size={"large"} block>
                            Сохранить
                        </Button>
                    </Form.Item>
                </Form>
            </Card>
        </Page>
    );
};

export default UpdateSharePage;