import React, {useContext, useEffect, useState} from 'react';
import {Link, useNavigate, useParams} from "react-router-dom";
import {Context} from "../../../index";
import Page from "../../../components/template/Page/Page";
import cl from './SharePage.module.css';
import Graphs from "../../../components/graph/Graphs/Graphs";

const SharePage = () => {

    const {store} = useContext(Context);
    const {code} = useParams();
    const [page, setPage] = useState();
    const navigate = useNavigate();

    useEffect(() => {
        const fetchData = async () => {
            const response = await store.sharePages.getPageDashboard(code);
            if (!response) {
                navigate("/404")
            }
            setPage(response)
        }
        fetchData()
    }, [
        navigate,
        store.sharePages,
        setPage,
        code
    ]);

    return (
        <>
            <Page>
                <div className={cl.title}>{page?.sharePage?.title}</div>
                <div className={cl.desc}>{page?.sharePage?.description}</div>
                <div className={cl.infoBox}>
                    <div className={cl.infoTitle}>Ссылка на сайт:</div>
                    <Link to={page?.baseUrl} className={cl.infoDesc} target={"_blank"}>{page?.baseUrl}</Link>
                </div>
                <Graphs graphs={page?.graphs}/>
            </Page>
        </>
    );
};

export default SharePage;