import React from 'react';
import {Button, Empty} from "antd";
import cl from './Error404.module.css';

const Error404 = () => {
    return (
        <div className={cl.box}>
            <div className={cl.title}>Ошибка 404</div>
            <Empty className={cl.desc}
                description={"Кажется что-то пошло не так! Страница, которую вы запрашиваете, не существует. " +
                    "Возможно она устарела, была удалена, или был введен неверный адрес в адресной строке."}/>
            <Button type={"primary"} size={"large"} href={"/"}>На главную</Button>
        </div>
    );
};

export default Error404;