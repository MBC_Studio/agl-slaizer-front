import React, {useContext} from 'react';
import cl from './Home.module.css';
import preview from '../../images/preview.png'
import {Link} from "react-router-dom";
import Page from "../../components/template/Page/Page";
import LandingCard from "../../components/template/LandingCard/LandingCard";

import research from '../../images/research.png';
import stats from '../../images/stats.png';
import mail from '../../images/mail.png';
import archive from '../../images/archive.png';
import post from '../../images/post.png';
import projects from '../../images/projects.png';
import {Context} from "../../index";


const Home = () => {

    const {store} = useContext(Context)

    return (
        <>
            <img src={preview} alt={"preview"} className={cl.previewImg}/>
            <Page>
                <div className={cl.preview}>
                    <h1 className={cl.title}>WebPulse Analytics</h1>
                    <div className={cl.description}>Мощный инструмент для контроля доступности вашего веб-сайта.
                        Обезопасьте
                        свой сайт от выхода из строя и получайте уведомления в случае возникновения проблем.
                    </div>
                    {
                        store.isAuth ?
                            <Link className={cl.btn} to={"/projects"}>
                                Перейти к проектам
                            </Link>
                            :
                            <Link className={cl.btn} to={"/login"}>
                                Перейти к проектам
                            </Link>
                    }
                </div>

                <div className={cl.blocksTitle}>Возможности</div>
                <div className={cl.blockBox}>
                    <LandingCard title={"Мониторинг"}
                                 icon={research}
                                 description={"Круглосуточная автоматическая проверка с широким выбором параметров мониторинга," +
                                     " чтобы каждый пользователь мог настроить его под свои потребности и требования."}
                    />
                    <LandingCard title={"Статистика"}
                                 icon={stats}
                                 description={"Контролируйте доступность сайта и время отклика на выбранных вами графиках."}
                    />
                    <LandingCard title={"Уведомления"}
                                 icon={mail}
                                 description={"В случае обнаружения сбоя отправляем уведомление на email или в Telegram."}
                    />
                </div>
                <div className={cl.blockBox}>
                    <LandingCard title={"Архив инцидентов"}
                                 icon={archive}
                                 description={"Сохраняем информацию о возникавших инцидентах, времени начала, продолжительности и текущем статусе."}
                    />
                    <LandingCard title={"Публичные страницы"}
                                 icon={post}
                                 description={"Возможность публиковать проекты для просмотра графиков другим пользователям."}
                    />
                    <LandingCard title={"Групповое управление"}
                                 icon={projects}
                                 description={"Управляйте несколькими проектами с помощью одного аккаунта и центра управления для удобства организации работы."}
                    />
                </div>
            </Page>
        </>
    );
};

export default Home;