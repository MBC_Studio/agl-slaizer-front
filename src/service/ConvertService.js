export default class ConvertService {
    static convertUserStatus = (status: string): string => {
        const translations: {[key: string]: string} = {
            ACTIVE: 'Активный',
            BLOCKED: 'Заблокирован',
            WAITING: 'Ожидает подтверждения почты'
        };
        return translations[status] || 'Неизвестный статус';
    }

    // static convertDateToDate(unixTimestamp: number): string {
    //     const milliseconds = unixTimestamp * 1000;
    //     const localDate = new Date(milliseconds);
    //
    //     const options = {
    //         day: '2-digit',
    //         month: '2-digit',
    //         year: 'numeric',
    //     };
    //
    //     const formattedDate = localDate.toLocaleString('ru-RU', options);
    //     return formattedDate.charAt(0).toUpperCase() + formattedDate.slice(1);
    // }

    static convertDateToDate(dateTimeString) {
        const date = new Date(dateTimeString);

        const options = {
            day: '2-digit',
            month: '2-digit',
            year: 'numeric'
        };

        const formattedDateTime = date.toLocaleString('ru-RU', options);
        return formattedDateTime.charAt(0).toUpperCase() + formattedDateTime.slice(1);
    }

    static convertDateToDateTime(dateTimeString) {
        const date = new Date(dateTimeString);

        const options = {
            day: '2-digit',
            month: '2-digit',
            year: 'numeric',
            hour: '2-digit',
            minute: '2-digit'
        };

        const formattedDateTime = date.toLocaleString('ru-RU', options);
        return formattedDateTime.charAt(0).toUpperCase() + formattedDateTime.slice(1);
    }
}