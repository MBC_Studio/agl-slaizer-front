import type AppStore from "../AppStore";
import {makeAutoObservable} from "mobx";
import $api from "../../http";
import {message} from "antd";

export default class MetricStore {
    rootStore: AppStore;

    constructor(rootStore: AppStore) {
        makeAutoObservable(this);
        this.rootStore = rootStore;
    }

    getAll = async (projectId: number) => {
        try {
            const response = await $api.get(`/projects/${projectId}/metrics`)
            return response.data
        } catch (e) {
            this.rootStore.httpError(e)
        }
    }

    getById = async (projectId: number, metricId: number) => {
        try {
            const response = await $api.get(`/projects/${projectId}/metrics/${metricId}`)
            return response.data
        } catch (e) {
            this.rootStore.httpError(e);
        }
    }

    edit = async (projectId: number, metricId: number, values) => {
        try {
            await $api.put(`/projects/${projectId}/metrics/${metricId}`, values);
            message.success('Изменения сохранены');
            return true;
        } catch (e) {
            this.rootStore.httpError(e);
            return false;
        }
    }

    create = async (projectId: number, values) => {
        try {
             await $api.post(`/projects/${projectId}/metrics`, values);
            message.success('Метрика создана');
            return true;
        } catch (e) {
            this.rootStore.httpError(e);
            return false;
        }
    }

    delete = async (projectId: number, metricId: number) => {
        try {
            const response = await $api.delete(`/projects/${projectId}/metrics/${metricId}`);
            message.success('Метрика удалена');
            return response.data;
        } catch (e) {
            this.rootStore.httpError(e)
        }
    }
}