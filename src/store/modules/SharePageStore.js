import {makeAutoObservable} from "mobx";
import type AppStore from "../AppStore";
import $api from "../../http";
import {message} from "antd";

export default class SharePageStore {
    rootStore: AppStore;

    constructor(rootStore: AppStore) {
        makeAutoObservable(this);
        this.rootStore = rootStore;
    }

    getById = async (projectId) => {
        try {
            const response = await $api.get(`/projects/${projectId}/share-pages`);
            return response.data;
        } catch (e) {
            this.rootStore.httpError(e);
        }
    }

    edit = async (projectId, sharePageId, values) => {
        try {
            await $api.put(`/projects/${projectId}/share-pages/${sharePageId}`, values);
            message.success('Изменения сохранены');
            return true;
        } catch (e) {
            this.rootStore.httpError(e);
            return false;
        }
    }

    changeStatus = async (projectId, sharePageId, values) => {
        try {
            const json = JSON.stringify(values)
            await $api.patch(`/projects/${projectId}/share-pages/${sharePageId}/status`, json);
            message.success('Статус обновлен');
            return true;
        } catch (e) {
            this.rootStore.httpError(e);
            return false;
        }
    }

    delete = async (id) => {
        try {
            const response = await $api.delete(`/projects/${id}`);
            message.success('Проект удален');
            return response.data;
        } catch (e) {
            this.rootStore.httpError(e)
        }
    }

    getPageDashboard = async (projectCode) => {
        try {
            const response = await $api.get(`/share-pages/${projectCode}`);
            return response.data;
        } catch (e) {
            message.error('Публичная страница недоступна')
            return false;
        }
    }
}