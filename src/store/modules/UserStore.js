import {makeAutoObservable} from "mobx";
import AppStore from "../AppStore";
import $api from "../../http";
import {message} from "antd";

export default class UserStore {
    rootStore: AppStore;

    isLoading = false;

    constructor(rootStore: AppStore) {
        makeAutoObservable(this);
        this.rootStore = rootStore;
    }

    register = async (values) => {
        try {
            this.isLoading = true;
            delete values.confirm;
            const response = await $api.post('/auth/sign-up', values);
            localStorage.setItem('token', response.data.token);
            this.rootStore.loadUser(response.data.token);
            message.info('Добро пожаловать, ' + this.rootStore.user.username + "!");
        } catch (e) {
            this.rootStore.httpError(e);
        } finally {
            this.isLoading = false;
        }
    }

    login = async (values) => {
        try {
            this.isLoadingState = true;
            const response = await $api.post('/auth/sign-in', values);
            localStorage.setItem('token', response.data.token);
            this.rootStore.loadUser(response.data.token);

            message.info('С возращением, ' + this.rootStore.user.username + "!");
        } catch (e) {
            this.rootStore.httpError(e);
        } finally {
            this.isLoadingState = false;
        }
    }

    changePassword = async (data) => {
        try {
            const requestData = {
                oldPassword: data.oldPassword,
                newPassword: data.newPassword,
            };
            const json = JSON.stringify(requestData);
            await $api.put('/users/change-password', json);
            message.success('Пароль успешно изменен');
        } catch (e) {
            this.rootStore.httpError(e);
        }
    }

    getById = async (id: number) => {
        try {
            const response = await $api.get(`/admins/users/${id}`);
            return response.data;
        } catch (e) {
            this.rootStore.httpError(e);
        }
    }

    edit = async (id, data) => {
        try {
            const response = await $api.put(`/admins/users/${id}`, data);
            message.success('Пользователь обновлен');
            return response.data;
        } catch (e) {
            this.rootStore.httpError(e);
        }
    }

    getAll = async () => {
        try {
            const response = await $api.get(`/admins/users`);
            return response.data;
        } catch (e) {
            this.rootStore.httpError(e);
        }
    }

    delete = async (id) => {
        try {
            const response = await $api.delete(`/admins/users/${id}`);
            message.success('Пользователь удален');
            return response.data;
        } catch (e) {
            this.rootStore.httpError(e);
        }
    }

    logout = () => {
        localStorage.removeItem('token');
        this.user = null;
        this.isAuth = false;
        window.location.href = '/';
    }
}