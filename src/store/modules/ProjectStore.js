import {makeAutoObservable} from "mobx";
import type AppStore from "../AppStore";
import $api from "../../http";
import {message} from "antd";

export default class ProjectStore {
    rootStore: AppStore;

    constructor(rootStore: AppStore) {
        makeAutoObservable(this);
        this.rootStore = rootStore;
    }

    getAllByUser = async () => {
        try {
            const response = await $api.get('/projects');
            return response.data;
        } catch (e) {
            this.rootStore.httpError(e);
        }
    }

    getAll = async () => {
        try {
            const response = await $api.get('/admins/projects');
            return response.data;
        } catch (e) {
            this.rootStore.httpError(e);
        }
    }

    getById = async (id: number) => {
        try {
            const response = await $api.get(`/projects/${id}`);
            return response.data;
        } catch (e) {
            this.rootStore.httpError(e);
        }
    }

    getDashboard = async (projectId: number) => {
        try {
            const response = await $api.get(`/projects/${projectId}/dashboard`)
            return response.data
        } catch (e) {
            this.rootStore.httpError(e);
        }
    }

    edit = async (id, values) => {
        try {
            await $api.put(`/projects/${id}`, values);
            message.success('Изменения сохранены');
            return true;
        } catch (e) {
            this.rootStore.httpError(e);
            return false;
        }
    }

    create = async (values) => {
        try {
            await $api.post('/projects', values);
            message.success('Проект создан');
            return true;
        } catch (e) {
            this.rootStore.httpError(e);
            return false;
        }
    }

    delete = async (id) => {
        try {
            const response = await $api.delete(`/projects/${id}`);
            message.success('Проект удален');
            return response.data;
        } catch (e) {
            this.rootStore.httpError(e)
        }
    }

    changeStatus = async (id, data) => {
        try {
            const response = await $api.patch(`/admins/projects/${id}/status`, data);
            message.success('Статус проекта изменен');
            return response.data;
        } catch (e) {
            this.rootStore.httpError(e)
        }
    }
}