import {makeAutoObservable} from "mobx";
import UserStore from "./modules/UserStore";
import {message, notification} from "antd";
import ProjectStore from "./modules/ProjectStore";
import MetricStore from "./modules/MetricStore";
import $api from "../http";
import SharePageStore from "./modules/SharePageStore";

export default class AppStore {

    users = new UserStore(this);
    projects = new ProjectStore(this);
    metrics = new MetricStore(this);
    sharePages = new SharePageStore(this);

    constructor() {
        makeAutoObservable(this, {
                users: false,
            },
            {
                deep: true
            });

        // Проверка авторизации
        this.checkAuth();
    }

    user = null;
    isAuth = false;


    checkAuth = () => {
        const token = localStorage.getItem('token');
        if (token) {
            this.loadUser(token);
        }
    }

    loadUser = (token) => {
        const check = async () => {
            try {
                await $api.get('/users/check');
            } catch (e) {
                this.isAuth = false;
                this.user = null;
                localStorage.removeItem('token');

                message.error('Ваша сессия истекла. Пожалуйста, авторизуйтесь заново');
            }
        }
        const {id, email, role} = JSON.parse(atob(token.split('.')[1]));
        const username = JSON.parse(atob(token.split('.')[1])).sub;
        this.user = {id, username, email, role};
        this.isAuth = true;
        check();
    }

    httpError = (e) => {
        const msg = 'Ошибка сервера';
        if (e.response) {
            switch (e.response?.status) {
                case 400 || 401:
                    if (e.response.data?.title === 'Constraint Violation') {
                        notification.error({
                            message: 'Ошибка валидации данных',
                            description: e.response.data?.violations?.map(v => <p>{v.message}</p>),
                        }, 10);
                    } else {
                        notification.error({
                            message: e.response.data?.title,
                            description: e.response.data?.detail,
                        }, 10);
                    }
                    break;
                case 403:
                    message.error('Недостаточно прав');
                    break;
                case 404:
                    message.error('Не найдено');
                    break;
                default:
                    notification.error({
                        message: e.response.data.title,
                        description: e.response.data.detail,
                    }, 10);
            }
        } else {
            message.error(msg);
        }
    }

    isAdmin() {
        // return true;
        return this.user?.role === 'ROLE_ADMIN';
    }
};
