import './App.css';
import {Route, Routes} from "react-router-dom";
import LoginPage from "./pages/auth/LoginPage";
import RegisterPage from "./pages/auth/RegisterPage";
import Home from "./pages/Home/Home";
import CreateUpdateProject from "./pages/project/CreateUpdateProject/CreateUpdateProject";
import Account from "./pages/Account/Account";
import Projects from "./pages/project/Projects/Projects";
import Project from "./pages/project/ProjectDashboard/Project";
import CreateUpdateMetric from "./pages/metric/CreateUpdateMetric/CreateUpdateMetric";
import SharePage from "./pages/sharePage/SharePageDashboard/SharePage";
import UpdateSharePage from "./pages/sharePage/UpdateSharePage/UpdateSharePage";
import Error404 from "./pages/Error404/Error404";
import ProjectsAdmin from "./pages/admin/ProjectsAdmin";
import UsersAdmin from "./pages/admin/UsersAdmin";
import UpdateUser from "./pages/admin/UpdateUser";

function App() {
    return (
        <Routes>
            <Route path={"/"} element={<Home/>}/>
            <Route path={"/login"} element={<LoginPage/>}/>
            <Route path={"/register"} element={<RegisterPage/>}/>
            <Route path={"/projects"} element={<Projects/>}/>
            <Route path={"/projects/create"} element={<CreateUpdateProject/>}/>
            <Route path={"/projects/:projectId/update"} element={<CreateUpdateProject/>}/>
            <Route path={"/projects/:projectId"} element={<Project/>}/>
            <Route path={"/projects/:projectId/metrics/create"} element={<CreateUpdateMetric/>}/>
            <Route path={"/projects/:projectId/metrics/update/:metricId"} element={<CreateUpdateMetric/>}/>
            <Route path={"/projects/:projectId/share-pages/update"} element={<UpdateSharePage/>}/>
            <Route path={"/public/:code"} element={<SharePage/>}/>
            <Route path={"/account"} element={<Account/>}/>
            <Route path={"/404"} element={<Error404/>}/>
            <Route path={"/admin/projects"} element={<ProjectsAdmin/>}/>
            <Route path={"/admin/users"} element={<UsersAdmin/>}/>
            <Route path={"/admin/users/:userId/update"} element={<UpdateUser/>}/>
        </Routes>
    );
}

export default App;
